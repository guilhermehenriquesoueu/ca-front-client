const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  configureWebpack: {
    plugins: [ 
      new BundleAnalyzerPlugin({
        analyzerPort: 8889
      }),
      new PreloadWebpackPlugin({
        rel: 'preload',
        as: 'font',
        include: 'allAssets',
        fileWhitelist: [/\.(woff2?|eot|ttf|otf)(\?.*)?$/i],
      })
    ],
  },
  devServer: {
    port: 8888
  },
  pwa: {
    name: 'Canto Estatístico - A melhor plataforma de análise para mercado de cantos de futebol.', // <---- this is PWA name
  }
}
