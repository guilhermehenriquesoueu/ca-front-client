import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Password from '../views/Password.vue';
import Activation from '../views/Activation.vue';
import Links from '../views/Links.vue';
import axios from 'axios';
import store from '../store';
import xss from 'xss';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/links',
    name: 'Links',
    component: Links,
  },
  {
    path: '/app',
    name: 'App',
    component: () => import(/* webpackChunkName: "app-main" */ '../views/App.vue'),
    redirect: '/app/live',
    children: [
      {
        path: 'info',
        component: () => import(/* webpackChunkName: "info" */ '../views/Info.vue'),
      },
      {
        path: 'live',
        component: () => import(/* webpackChunkName: "live" */ '../components/CardList__live.vue'),
      },
      {
        path: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue'),
      },
      {
        path: 'wiki',
        component: () => import(/* webpackChunkName: "profile" */ '../views/Wiki.vue'),
      }
    ],
    beforeEnter(to, from, next) {
      const config = {
        method: 'get',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/validate`,
        withCredentials: true,
      }
      axios(config)
      .then(res => {
        store.commit('setDataUser', res.data);
        next();
      }).catch(error => {
        next('/login');
      });
    },
  },
  {
    path: '/password',
    name: 'Password',
    component: Password,
    beforeEnter(to, from, next){
      const e = xss(to.query.e);
      const t = xss(to.query.t);
      if(e === undefined || t === undefined) next('/');
      next();
    }
  },
  {
    path: '/activation',
    name: 'Activation',
    component: Activation,
    beforeEnter(to, from, next){
      const e = xss(to.query.e);
      const t = xss(to.query.t);
      if(e === undefined || t === undefined) next('/');
      const data = JSON.stringify({ email: e, token: t });
      const config = {
        method: 'get',
        url: `${process.env.VUE_APP_HOSTSERVICE}/api/user/${e}/${t}`,
        headers: {
          'Content-Type': 'application/json',
        },
        data,
      };
      axios(config).then((res) => {
        if(res.data.status == false) return next('/');
        next();
      }).catch(() => {
        return;
      });
    },
  },
  {
    path: "*",
    beforeEnter(to, from, next){
      next('/');
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
