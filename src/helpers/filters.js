import store  from '../store/index';
const filters = (data) => {
    const {filterNoFilter, filterAppm, filterCg, filterBoth} = store.getters.getUserInfo
    if(filterNoFilter == 'true'){
    const onlyFile = []
    data.map((el) => {
        if(el.appm_home >= 1 || el.appm_away >= 1){
        onlyFile.push(el)
        }
    })

    const noFile = []
    data.map((el) => {
        if(el.appm_home < 1 && el.appm_away < 1){
        noFile.push(el)
        }
    })
    const final = [...onlyFile, ...noFile]
    return final
    }
    if(filterAppm == 'true'){
        let higherAppm = [];
        data.map((el) => {
            if(el.appm_home >= 1 || el.appm_away >= 1){
                higherAppm.push(el);
            }
        })
        return higherAppm
    }

    if(filterCg == 'true'){
        let higherCg = [];
        data.map((el) => {
            if((el.cg_home >=10 || el.cg_away >=9) && el.timer.tm < 45) {
                higherCg.push(el);
            }
            if((el.cg_home >=15 || el.cg_away >=15) && el.timer.tm > 45){
                higherCg.push(el);
            }
        })
        return higherCg
    }

    if(filterBoth == 'true'){
        let boths = [];
        data.map((el) => {
            if((el.cg_home >=10 || el.cg_away >=10) && (el.appm_home >=1 || el.appm_away >=1) && el.timer.tm <= 45) {
                boths.push(el);
            }
            if((el.cg_home >=15 || el.cg_away >=15) && (el.appm_home >=1 || el.appm_away >=1) && el.timer.tm > 45){
                boths.push(el);
            }
        })
        return boths
    }
}

export default filters